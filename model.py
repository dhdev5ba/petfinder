import torch
import torch.nn as nn
from torchvision import models


def set_parameter_requires_grad(model, freeze):
    for param in model.parameters():
        param.requires_grad = not freeze


class PetFinderRegressor(nn.Module):
    
    def __init__(self, config):

        self.config = config
        
        super().__init__()
        
        self.model = models.resnet34(pretrained=config.use_pretrained)
        set_parameter_requires_grad(self.model, config.freeze)

        self.image_layers = nn.Sequential(
            nn.Linear(1000, 500),
            nn.ReLU(),
            nn.BatchNorm1d(500),
            nn.Linear(500, 100),
            nn.ReLU(),
            nn.BatchNorm1d(100),
            nn.Linear(100, 10),
            nn.ReLU(),
            nn.BatchNorm1d(10),
            nn.Linear(10, 1),
            nn.ReLU(),
            nn.BatchNorm1d(1),
        )
        
        self.feature_layers = nn.Sequential(
            nn.Linear(12, 6),
            nn.ReLU(),
            nn.BatchNorm1d(6),
            nn.Linear(6, 1),
            nn.ReLU(),
            nn.BatchNorm1d(1)
        )

        self.final_layers = nn.Sequential(
            nn.Linear(2, 1),
            nn.ReLU()
        )


    # def forward(self, image, feature):
    def forward(self, image, feature):
        image_result = self.model(image)
        image_result = self.image_layers(image_result)            # (batch, 1)

        feature_result = self.feature_layers(feature)             # (batch, 1)

        result = torch.cat([image_result, feature_result], dim=1) # (batch, 2)

        result = self.final_layers(result)

        return result * 100
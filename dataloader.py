import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms

from PIL import Image
import numpy as np

from sklearn.model_selection import train_test_split


torch.manual_seed(2021)


class PetFinderDataset(Dataset):
    
    def __init__(self, img_path, feature, pawpularity, transform=None):
        self.img_path = img_path
        self.feature = feature
        self.pawpularity = pawpularity
        self.transform = transform
        
    def __len__(self):
        return self.img_path.shape[0]
    
    def __getitem__(self, idx):
        image = Image.open(self.img_path.iloc[idx])

        if self.transform:
            image = self.transform(image)
        
        feature = torch.from_numpy(self.feature.iloc[idx].values)
        
        pawpularity = torch.tensor(self.pawpularity.iloc[idx], dtype=torch.float64)
        
        return image, feature, pawpularity


def load_dataset():
    import pandas as pd

    train_df = pd.read_csv('train.csv')
    train_df['Id'] = './train/' + train_df['Id'].astype(str) + '.jpg'

    return train_df


def get_loaders(config):
    dataset = load_dataset()

    train, valid = train_test_split(dataset, train_size=config.train_ratio, random_state=2021)

    train_loader = DataLoader(
        dataset=PetFinderDataset(
            train['Id'],
            train[train.columns[1:-1]],
            train['Pawpularity'],
            transform=transforms.Compose([
                # transforms.RandomResizedCrop(224),
                transforms.Resize((224, 224)),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor()
            ])),
        batch_size=config.batch_size,
        shuffle=True
    )
    valid_loader = DataLoader(
        dataset=PetFinderDataset(
            valid['Id'],
            valid[valid.columns[1:-1]],
            valid['Pawpularity'],
            transform=transforms.Compose([
                transforms.Resize((224, 224)),
                transforms.ToTensor()
            ])),
        batch_size=config.batch_size,
        shuffle=True
    )

    return train_loader, valid_loader
from copy import deepcopy
from os import stat

import numpy as np

import torch
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.utils as torch_utils

from ignite.engine import Engine
from ignite.engine import Events
from ignite.metrics import RunningAverage
from ignite.contrib.handlers.tqdm_logger import ProgressBar

from utils import get_grad_norm, get_parameter_norm

import wandb


VERBOSE_SILENT = 0
VERBOSE_EPOCH_WISE = 1
VERBOSE_BATCH_WISE = 2

torch.manual_seed(2021)


class MyEngine(Engine):

    def __init__(self, func, model, crit, optimizer, config):
        # Ignite Engine does not have objects in below lines.
        # Thus, we assign class variables to access these object, during the procedure.
        self.model = model
        self.crit = crit
        self.optimizer = optimizer
        self.config = config

        super().__init__(func) # Ignite Engine only needs function to run.

        self.best_loss = np.inf
        self.best_model = None

        self.device = next(model.parameters()).device

    @staticmethod
    def train(engine, mini_batch):
        # You have to reset the gradients of all model parameters
        # before to take another step in gradient descent.
        engine.model.train() # Because we assign model as class variable, we can easily access to it.
        engine.optimizer.zero_grad()

        image, feature, pawpularity = mini_batch

        image  = image.to(engine.device).float()
        feature = feature.to(engine.device).float()
        pawpularity = pawpularity.to(engine.device).float()

        # Teke feed-forward
        y_hat = engine.model(image, feature)

        loss = engine.crit(y_hat, pawpularity.view(pawpularity.size(0), -1))
        loss.backward()

        # Calculate accuracy only if 'y' is LongTensor,
        # which means that 'y' is one-hot representation.
        if isinstance(pawpularity, torch.LongTensor) or isinstance(pawpularity, torch.cuda.LongTensor):
            accuracy = (torch.argmax(y_hat, dim=-1) == pawpularity).sum() / float(pawpularity.size(0))
        else:
            accuracy = 0
        
        p_norm = float(get_parameter_norm(engine.model.parameters()))
        g_norm = float(get_grad_norm(engine.model.parameters()))

        # Take a step of gradient descent.
        engine.optimizer.step()

        return {
            'loss': float(loss),
            'accuracy': float(accuracy),
            '|param|': p_norm,
            '|g_param|': g_norm
        }
    
    @staticmethod
    def validate(engine, mini_batch):
        engine.model.eval()

        with torch.no_grad():
            image, feature, pawpularity = mini_batch

            image  = image.to(engine.device).float()
            feature = feature.to(engine.device).float()
            pawpularity = pawpularity.to(engine.device).float()

            y_hat = engine.model(image, feature)

            loss = engine.crit(y_hat, pawpularity.view(pawpularity.size(0), -1))

            if isinstance(pawpularity, torch.LongTensor) or isinstance(pawpularity, torch.cuda.LongTensor):
                accuracy = (torch.argmax(y_hat, dim=-1) == pawpularity).sum() / float(pawpularity.size(0))
            else:
                accuracy = 0
            
        return {
            'loss': float(loss),
            'accuracy': float(accuracy)
        }
    
    @staticmethod
    def attach(train_engine, validation_engine, verbose=VERBOSE_BATCH_WISE):
        # Attaching would be repeated for several metrics.
        # Thus, we can reduce the repeated codes by using this function.
        def attach_running_average(engine, metric_name):
            RunningAverage(output_transform=lambda x: x[metric_name]).attach(
                engine,
                metric_name
            )
        training_metric_names = ['loss', 'accuracy', '|param|', '|g_param|']

        for metric_name in training_metric_names:
            attach_running_average(train_engine, metric_name)

        # If the verbosity is set, progress bar would be shown for mini-batch iterations.
        # Without ignite, you can use tqdm to implement progress bar.
        if verbose >= VERBOSE_BATCH_WISE:
            pbar = ProgressBar(bar_format=None, ncols=120)
            pbar.attach(train_engine, training_metric_names)

        # If the verbosity is set, statistics would be shown after each epoch.
        if verbose >= VERBOSE_EPOCH_WISE:
            @train_engine.on(Events.EPOCH_COMPLETED)
            def print_train_logs(engine):
                wandb.log({
                    'param': engine.state.metrics["|param|"],
                    'g_param': engine.state.metrics["|g_param|"],
                    'train_loss': engine.state.metrics["loss"]
                })
                print(f'Epoch {engine.state.epoch} - |param|={engine.state.metrics["|param|"]:.4f}  |g_param|={engine.state.metrics["|g_param|"]}  loss={engine.state.metrics["loss"]:.4f}  accuracy={engine.state.metrics["accuracy"]:.4f}')
        
        
        validation_metric_names = ['loss', 'accuracy']

        for metric_name in validation_metric_names:
            attach_running_average(validation_engine, metric_name)
        
        # Do same things for validaion engine.
        if verbose >= VERBOSE_BATCH_WISE:
            pbar = ProgressBar(bar_format=None, ncols=120)
            pbar.attach(validation_engine, validation_metric_names)
        
        if verbose >= VERBOSE_EPOCH_WISE:
            @validation_engine.on(Events.EPOCH_COMPLETED)
            def print_valid_logs(engine):
                wandb.log({
                    'valid_loss': engine.state.metrics["loss"],
                    'best_loss': engine.best_loss
                })
                print(f'Validation - loss={engine.state.metrics["loss"]:.4f}  accuracy={engine.state.metrics["accuracy"]:.4f}  best_loss={engine.best_loss:.4f}')
        
    @staticmethod
    def check_best(engine):
        loss = float(engine.state.metrics['loss'])
        if loss <= engine.best_loss:                                # If current epoch returns lower validation loss,
            engine.best_loss = loss                                 # Update lowest validation loss.
            engine.best_model = deepcopy(engine.model.state_dict()) # Update best model weights.

    @staticmethod
    def save_model(engine, train_engine, config, **kwargs):
        torch.save(
            {
                'model': engine.best_model,
                'config': config,
                **kwargs
            }, config.model_fn
        )


class Trainer():

    def __init__(self, config):
        self.config = config
    
    def train(
        self,
        model, crit, optimizer,
        train_loader, valid_loader
    ):
        train_engine = MyEngine(
            MyEngine.train,
            model, crit, optimizer, self.config
        )
        validation_engine = MyEngine(
            MyEngine.validate,
            model, crit, optimizer, self.config
        )
        MyEngine.attach(
            train_engine,
            validation_engine,
            verbose=self.config.verbose
        )

        def run_validation(engine, validation_engine, valid_loader):
            validation_engine.run(valid_loader, max_epochs=1)
        
        train_engine.add_event_handler(
            Events.EPOCH_COMPLETED,         # event
            run_validation,                 # function
            validation_engine, valid_loader # arguments
        )
        validation_engine.add_event_handler(
            Events.EPOCH_COMPLETED,         # event
            MyEngine.check_best             # function
        )
        validation_engine.add_event_handler(
            Events.EPOCH_COMPLETED,         # event
            MyEngine.save_model,            # function
            train_engine, self.config       # arguments
        )

        train_engine.run(
            train_loader,
            max_epochs=self.config.n_epochs
        )

        return model